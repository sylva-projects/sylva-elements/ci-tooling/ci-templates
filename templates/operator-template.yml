---
# -----------------------------------------------------------------------------
# Gitlab-ci template for operator in Sylva-projects
# -----------------------------------------------------------------------------

stages:
  - test

# -----------------------------------------------------------------------------
# Check if the manifests are generated
# -----------------------------------------------------------------------------

ensure-manifests-are-generated:
  stage: test
  image: $GO_IMAGE
  interruptible: true
  needs: []
  script:
    - make manifests
    - make generate
    - |
      if ! git diff --exit-code; then
        echo -e "\e[0;31mManifests are not generated, please run make build before pushing your changes\e[0m"
        exit 1
      fi
  rules:
    - if: $CHECK_MANIFEST_DISABLED == 'true' || $CHECK_MANIFEST_DISABLED == '1'
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

# -----------------------------------------------------------------------------
# Try to install and deploy the operator in a kind cluster
# Use make install & make deploy
# Check if $OPERATOR_DEPLOYMENT deployment is ready in $OPERATOR_NAMESPACE namespace
# -----------------------------------------------------------------------------

deploy-operator:
  image: registry.gitlab.com/sylva-projects/sylva-elements/container-images/ci-image/ci-operator-image:v1.1.1
  stage: test
  interruptible: true
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - name: docker:dind
      alias: kubernetes
  dependencies: []
  script:
    - |
      echo -e "\e[1m\e[0Ksection_start:`date +%s`:setup_kind[collapsed=true]\r\e[0KCreating Kind cluster\e[0m"
      kind create cluster --config=/kind_config.yaml
      sed -i -E -e 's/localhost|0\.0\.0\.0/kubernetes/g' "$HOME/.kube/config"
      kubectl get nodes -o wide
      echo -e "\e[0Ksection_end:`date +%s`:setup_kind\r\e[0K"
    - |
      echo -e "\e[1m\e[0Ksection_start:`date +%s`:deploy_crd[collapsed=true]\r\e[0KInstall the CRDs into the cluster\e[0m"
      kustomize build config/crd | kubectl apply -f -
      echo -e "\e[0Ksection_end:`date +%s`:deploy_crd\r\e[0K"
    - |
      echo -e "\e[1m\e[0Ksection_start:`date +%s`:deploy_controller[collapsed=true]\r\e[0KDeploy controller\e[0m"
      (cd config/manager; kustomize edit set image controller=${OPERATOR_DOCKER_IMAGE})
      kustomize build config/default | kubectl apply -f -
      echo -e "\e[0Ksection_end:`date +%s`:deploy_controller\r\e[0K"
    - |
      echo -e "\e[1m\e[0Ksection_start:`date +%s`:wait_ressources\r\e[0KWaiting for ressources to be ready\e[0m"
      kubectl rollout status deployment -n ${OPERATOR_NAMESPACE} ${OPERATOR_DEPLOYMENT} --timeout=90s
      kubectl get po -A
      echo ""
      kubectl get deployment -A
      echo -e "\e[0Ksection_end:`date +%s`:wait_ressources\r\e[0K"
  rules:
    - if: $DEPLOY_OPERATOR_DISABLED == 'true' || $DEPLOY_OPERATOR_DISABLED == '1'
      when: never
    # Following rules are the same as in our docker template.
    # They can be overriden if needed to match specific usecases.
    - if: $CI_COMMIT_TAG
      variables:
        OPERATOR_DOCKER_IMAGE: $IMAGE_REGISTRY:$CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'web'
      variables:
        OPERATOR_DOCKER_IMAGE: $IMAGE_REGISTRY/snapshot:0.0.0-git-$CI_COMMIT_SHORT_SHA

test-operator:
  image:  $GO_IMAGE
  stage: test
  interruptible: true
  dependencies: []
  variables:
    ENVTEST_K8S_VERSION: 1.30.0
  script:
    - make test
  rules:
    - if: $TEST_OPERATOR_DISABLED == 'true' || $TEST_OPERATOR_DISABLED == '1'
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

# CI templates

This repository contains multiple gitlab-ci templates.

## Operator-template

This template performs a quick test to ensure developers run

    - make manifests
    - make generate

commands before pushing their code.

It also try to deploy and install the operator in a kind cluster. For that you need to provide the follwoing variables:

* $OPERATOR_DEPLOYMENT
* $OPERATOR_NAMESPACE

It can be added to operator repo via an include, e.g.:

```yaml
include:
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: 1.0.0
    file: '/templates/operator-template.yml'

```

## Docker template

This template is used to create a building chain for docker container.

It performs:

- Dockerfile linting with Hadolint
- Building the container with docker buildx to include an SBOM and a provenance attestation as a layer of the container
- Signing the container if a COSIGN key is present and push the signature in the repo
- Running a security scan with gitlab tooling

By default, containers built in MR are pushed in a temporary namespace `$CI_REGISTRY_IMAGE/snapshot` & containers built by a tag event are pushed in `$CI_REGISTRY_IMAGE`

It can be added to a repository via an include, e.g.:

```yaml
include:
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: 1.0.0
    file: '/templates/docker-build.yml'
```

For repository containing multiple Dockerfile, we can add an override like this:

```yaml
.docker-base:
  parallel:
    matrix:
    - IMAGE_REGISTRY: $CI_REGISTRY_IMAGE
      DOCKERFILE: Dockerfile
    - IMAGE_REGISTRY: $CI_REGISTRY_IMAGE/test
      DOCKERFILE: Dockerfile-2
```

and all the Dockerfile will be built in parallel.

It's also possible to build containers sequentially, but it will require more overrides.

## Release notes template

This template is used to create gitlab release notes. It used a dedicated CI image with a custom script to generate a release notes.

It can be added to a repository via an include, e.g.:

```yaml
include:
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: 1.0.0
    file: '/templates/release-notes.yml'
```

It required a RELEASE_NOTES_READ_API_TOKEN variable set with a read only access token to use the gitlab Merge request API.

The release notes will by default display all the MR merge since the previous tag.

`.release-notes-before-script:`

`.release-notes-after-script:`

can be used to add modify the release notes if needed. These scripts can write content in `release-notes.md` file.

## Helm template

This template performs:

- helm-lint
- helm-yamllint
- helm-template-yamllint
- helm-schema-validation


It can be added to a repository via an include, e.g.:

```yaml
include:
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: 1.0.0
    file: '/templates/helm-tooling.yml'
```
#### Options

- `CHART_DIR`: Path to be checked (required).
- `YAMLLINT_CONFIG`: Yamllint file configuration (option) and by default `tools/common/configuration/yamllint-helm.yaml`
- `TEST_VALUES`: default values file tested by `helm lint` 
- `ADDITIONNAL_REPO_CHARTS` :  default values is empty otherwise list for example like this "bitnami=https://charts.bitnami.com/bitnami apache-airflow=https://airflow.apache.org"

The scripts above can be easily included in a GitLab pipeline, utilizing a specified Docker image located in registry.gitlab.com/sylva-projects/sylva-elements/container-images/helm-toolbox/.

